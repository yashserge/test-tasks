class NinjaTurtle:
    band_color = None
    weapon = None

class Leonardo(NinjaTurtle):
    def __init__(self):
        self.band_color = "blue"
        self.weapon = "two katanas"

class Michelangelo(NinjaTurtle):
    def __init__(self):
        self.band_color = "orange"
        self.weapon = "nunchuks"

class Donatello(NinjaTurtle):
    def __init__(self):
        self.band_color = "purple"
        self.weapon = "bo staff"

class Raphael(NinjaTurtle):
    def __init__(self):
        self.band_color = "red"
        self.weapon = "pair of sais"

class Creator:
    def create(self, type_ = None):
        raise NotImplementedError()

class TurtleCreator(Creator):
    def create(self, type_ = ""):
        if type_ == "Leonardo":
            return Leonardo()
        elif type_ == "Michelangelo":
            return Michelangelo()
        elif type_ == "Donatello":
            return Donatello()
        elif type_ == "Raphael":
            return Raphael()

creator = TurtleCreator()
leo = creator.create("Leonardo")
mic = creator.create("Michelangelo")
don = creator.create("Donatello")
rap = creator.create("Raphael")

print(leo.band_color)
print(leo.weapon)
print(mic.band_color)
print(mic.weapon)
print(don.band_color)
print(don.weapon)
print(rap.band_color)
print(rap.weapon)
print("------------------")

class Splinter:
    students = {}

    def __init__(self, *turtles):
        for turtle in turtles:
            self.students[type(turtle).__name__] = turtle

    def get_student_band_color(self, name):
        return self.students[name].band_color

    def get_student_weapon(self, name):
        return self.students[name].weapon

spl = Splinter(leo,mic,don,rap)
for name in spl.students:
    print(spl.get_student_band_color(name))
    print(spl.get_student_weapon(name))
