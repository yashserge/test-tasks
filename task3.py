import vk_api
import twitter #python-twitter
import pytest

class Connector(type):
    login = None
    password = None
    _instances = {}

    def __call__(cls,*args,**kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Connector,cls).__call__(*args,**kwargs)
        return cls._instances[cls]
    
    def get_profile(target):
        pass

    def get_friends(target):
        pass

    def get_posts(target):
        pass

class VkConnector(metaclass=Connector):
    def __init__(self,login,password):
        self.login,self.password = login,password
        
    def auth(self,login,password):
        vk_session = vk_api.VkApi(self.login,self.password)
        try:
            vk_session.auth(token_only=True)
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return

    def get_profile(self,target=562183230):
        vk_session = vk_api.VkApi(self.login,self.password)
        try:
            vk_session.auth(token_only=True)
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return
        vk = vk_session.get_api()
        profile = vk.users.get(user_ids=[target])
        return profile

    def get_posts(self,target=562183230):
        vk_session = vk_api.VkApi(self.login,self.password)
        try:
            vk_session.auth(token_only=True)
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return
        tools = vk_api.VkTools(vk_session)
        if not isinstance(target,int):
            profile = self.get_profile(target)
            if not len(profile):
                return
            target = profile[0]['id']
        wall = tools.get_all('wall.get', 100, {'owner_id': target})
        print('Posts count:', wall['count'])
        if wall['count']:
            return wall['items']

    def get_friends(self,target=562183230):
        vk_session = vk_api.VkApi(self.login,self.password)
        try:
            vk_session.auth(token_only=True)
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return
        if not isinstance(target,int):
            profile = self.get_profile(target)
            if not len(profile):
                return
            target = profile[0]['id']
        vk = vk_session.get_api()
        friends = vk.friends.get(user_id=target)
        print('Friends count:', friends['count'])
        if friends['count']:
            return friends['items']

class TwitterConnector(metaclass=Connector):
    access_token_key = None
    access_token_secret = None

    def __init__(self,login,password,access_token_key,access_token_secret):
        self.login,self.password = login,password
        self.access_token_key,self.access_token_secret = access_token_key,access_token_secret

    def get_profile(self,target="User53856429"):
        api = twitter.Api(self.login,self.password,self.access_token_key,self.access_token_secret)
        profile = api.GetUser(target)
        return profile

    def get_posts(self,target="User53856429"):
        api = twitter.Api(self.login,self.password,self.access_token_key,self.access_token_secret)
        posts = api.GetReplies()
        return posts

    def get_friends(self,target="User53856429"):
        api = twitter.Api(self.login,self.password,self.access_token_key,self.access_token_secret)
        users = api.GetFriends()
        followers = api.GetFollowers()
        return users

def test_singleton(login,password,key=None,secret=None):
    if key and secret:
        tw_1 = TwitterConnector(login,password,key,secret)
        tw_2 = TwitterConnector(login,password,key,secret)
        assert tw_1 is tw_2
    else:
        vk_1 = VkConnector(login,password)
        vk_2 = VkConnector(login,password)
        assert vk_1 is vk_2

vk_login,vk_password = list(input().split(' '))
print(vk_login,vk_password)
test_singleton(vk_login,vk_password)
vk_connector = VkConnector(vk_login,vk_password)
vk_connector_2 = VkConnector(vk_login,vk_password)
print(vk_connector is vk_connector_2)
print(vk_connector.get_profile())
print(vk_connector.get_friends())
print(vk_connector.get_posts())
print("------------------")
print(vk_connector.get_profile(1))
print(vk_connector.get_friends(1))
print(vk_connector.get_posts(1))
print("------------------")
print(vk_connector.get_profile("cactusnatus"))
print(vk_connector.get_friends("cactusnatus"))
print(vk_connector.get_posts("cactusnatus"))
print("------------------")

twitter_login,twitter_password = list(input().split(' '))
print(twitter_login,twitter_password)
access_token_key,access_token_secret = list(input().split(' '))
print(access_token_key,access_token_secret)
test_singleton(twitter_login,twitter_password,access_token_key,access_token_secret)
twitter_connector = TwitterConnector(twitter_login,twitter_password,access_token_key,access_token_secret)
twitter_connector_2 = TwitterConnector(twitter_login,twitter_password,access_token_key,access_token_secret)
print(twitter_connector is twitter_connector_2)
print(twitter_connector.get_profile())
print(twitter_connector.get_friends())
print(twitter_connector.get_posts())
