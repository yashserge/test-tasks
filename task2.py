import pytest

keys = (11,13,17,19,23,29,31,37,41,43)

class MyHash:
    steps = 3
    separator = ''

    def __init__(self,count = 3,symbol = ''):
        self.steps = count
        self.separator = symbol

    def get_hash(self,blocks,step = 1):
        result = ''
        s = 0
        for data in blocks:
            key = keys[step % len(keys)]
            for char in reversed(data):
                o = ord(char)
                s += o * key
                key *= key
        s %= words_count
        result = l[s]
        if step < self.steps:
            result += self.separator + self.get_hash(data,step+1)
        return result

    def F(self,obj):
        s = str(obj)
        block_size = 8
        blocks = [s[i:i+block_size] for i in range (0,len(s),block_size)]
        return self.get_hash(blocks)

def test_same_input(h,data):
    assert h.F(data) == h.F(data)

file_name = "words.txt"
with open(file_name, 'r') as file:
    l = [line.strip() for line in file]
    words_count = len(l)

    h0 = MyHash()
    test_same_input(h0,'vasya')
    print(h0.F("vasya"))
    print(h0.F('vasya'))
    print(h0.F([1,2,3]))
    test_same_input(h0,(11,13,17,19,23,29,31,37,41,43))
    print(h0.F((11,13,17,19,23,29,31,37,41,43)))
    print(h0.F({"QKRK!", None, True, False, 100500}))
    print(h0.F({"QKRK!": "Puppe", None: True, False : None, 100500: '3.14isDUKE'}))
    print(h0.F("naruto really wants to become the hogake"))
    print("------------------")

    h1 = MyHash(11,'-')
    test_same_input(h1,'vasya')
    print(h1.F("vasya"))
    print(h1.F('vasya'))
    print(h1.F([1,2,3]))
    test_same_input(h1,(11,13,17,19,23,29,31,37,41,43))
    print(h1.F((11,13,17,19,23,29,31,37,41,43)))
    print(h1.F({"QKRK!", None, True, False, 100500}))
    print(h1.F({"QKRK!": "Puppe", None: True, False : None, 100500: '3.14isDUKE'}))
    print(h1.F("naruto really wants to become the hogake"))
    print("------------------")

    h2 = MyHash(symbol='_')
    test_same_input(h2,'vasya')
    print(h2.F("vasya"))
    print(h2.F('vasya'))
    print(h2.F([1,2,3]))
    test_same_input(h2,(11,13,17,19,23,29,31,37,41,43))
    print(h2.F((11,13,17,19,23,29,31,37,41,43)))
    print(h2.F({"QKRK!", None, True, False, 100500}))
    print(h2.F({"QKRK!": "Puppe", None: True, False : None, 100500: '3.14isDUKE'}))
    print(h2.F("naruto really wants to become the hogake"))
    print("------------------")

    h3 = MyHash(count=2,symbol='[:||:]')
    test_same_input(h3,'vasya')
    print(h3.F("vasya"))
    print(h3.F('vasya'))
    print(h3.F([1,2,3]))
    test_same_input(h3,(11,13,17,19,23,29,31,37,41,43))
    print(h3.F((11,13,17,19,23,29,31,37,41,43)))
    print(h3.F({"QKRK!", None, True, False, 100500}))
    print(h3.F({"QKRK!": "Puppe", None: True, False : None, 100500: '3.14isDUKE'}))
    print(h3.F("naruto really wants to become the hogake"))
